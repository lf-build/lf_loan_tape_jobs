var Q = require('q');
var unirest = require('unirest');

function SecureHttp(token) {
  this.token = token;
}

SecureHttp.prototype.get = function get(service, path) {
  var deferred = Q.defer();
  var url = buildUrl(service, path);
  unirest.get(url)
    .header('Authorization', this.token)
    .header('Accept', 'application/json')
    .header('Content-Type', 'application/json')
    .send()
    .end(function (response) {
      if(response.status >= 200 && response.status < 300) {
        deferred.resolve(response);
      } else {
        deferred.reject(new Error("Request failed: GET " + path + ": " + JSON.stringify(response.error)));
      }
    });
  return deferred.promise;
}

SecureHttp.prototype.post = function post(service, path, body) {
  var deferred = Q.defer();
  var url = buildUrl(service, path);
  unirest.post(url)
    .header('Authorization', this.token)
    .header('Accept', 'application/json')
    .header('Content-Type', 'application/json')
    .send(JSON.stringify(body))
    .end(function (response) {
      if(response.status >= 200 && response.status < 300) {
        deferred.resolve(response);
      } else {
        deferred.reject(new Error("Request failed: POST " + path + ":" + JSON.stringify(response.error)));
      }
    });
  return deferred.promise;
};

function buildUrl(service, path) {
  if(typeof service === 'undefined' || service === null) throw new Error("Argument for parameter 'service' is null or undefined");
  if(typeof service.host === 'undefined' || service.host === null) throw new Error("Argument for parameter 'service' is null or undefined");
  var url = 'http://' + service.host;
  if (service.port) url += ':' + service.port;
  if (service.root) url += service.root;
  if (path) url += path;
  return url;
}

module.exports = SecureHttp;
