var moment = require('moment');

function Logger(name) {
  this.name = name;
}

Logger.prototype.info = function() {
  arguments[0] = moment().toISOString() + " {" + this.name + "} [INFO] " + arguments[0];
  console.log.apply(console, arguments);
}

Logger.prototype.error = function() {
  arguments[0] = moment().toISOString() + " {" + this.name + "} [ERROR] " + arguments[0];
  console.log.apply(console, arguments);
}

module.exports = Logger;
