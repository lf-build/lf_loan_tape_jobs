var fs = require('fs');
var jwt = require('jsonwebtoken');
var moment = require('moment-timezone');
var request = require('request');
var util = require('util');
var Q = require('q');

var Box = function(configuration) {
  if(!configuration) throw new Error('missing parameter: configuration');
  if(!configuration.userId) throw new Error('missing parameter: configuration.userId');
  if(!configuration.client) throw new Error('missing parameter: configuration.client');
  if(!configuration.client.id) throw new Error('missing parameter: configuration.client.id');
  if(!configuration.client.secret) throw new Error('missing parameter: configuration.client.secret');
  if(!configuration.keys) throw new Error('missing parameter: configuration.keys');
  if(!configuration.keys.id) throw new Error('missing parameter: configuration.keys.id');
  if(!configuration.keys.prv) throw new Error('missing parameter: configuration.keys.prv');
  if(!configuration.timezone) throw new Error('missing parameter: configuration.timezone');

  this.configuration = {
    userId: configuration.userId,
    client: {
      id: configuration.client.id,
      secret: configuration.client.secret,
    },
    keys: {
      id: configuration.keys.id,
      prv: configuration.keys.prv
    },
    timezone: configuration.timezone
  };
  this.token = null;
};

Box.prototype.isTokenExpired = function isTokenExpired() {
  return this.token === null || this.token.expiration.isSameOrBefore(moment());
};

Box.prototype.getToken = function getToken() {
  if(this.isTokenExpired()) {
    var self = this;
    return this.requestToken().then(function(token) {
      return self.token = token;
    });
  }
  else {
    return Q(this.token);
  }
};

Box.prototype.requestToken = function requestToken() {
  var systemTimeOffset = this.configuration.systemTimeOffset || 0;

  var payload = {
    'iss': this.configuration.client.id,
    'sub': this.configuration.userId,
    'box_sub_type': 'user',
    'aud': 'https://api.box.com/oauth2/token',
    'jti': rndstr(64),
    'exp': moment().tz(this.configuration.timezone).add(60 + systemTimeOffset, 'seconds').unix(),
    'nbf': moment().tz(this.configuration.timezone).add(-5 + systemTimeOffset, 'seconds').unix(),
    'iat': moment().tz(this.configuration.timezone).add(-5 + systemTimeOffset, 'seconds').unix()
  }

  var token = jwt.sign(payload, this.configuration.keys.prv, {
    algorithm: 'RS256',
    header: { kid: this.configuration.keys.id }
  });

  var url = 'https://api.box.com/oauth2/token';
  var form = {
    'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
    'client_id': this.configuration.client.id,
    'client_secret': this.configuration.client.secret,
    'assertion': token
  };

  var deferred = Q.defer();
  var requestTime = moment();

  request.post({url: url, form: form}, function(err, response, body) {
    var statusCode = parseInt(response.statusCode);
    if (statusCode !== 200)
      return deferred.reject(new Error(util.format('Box api error: status=%d error="%s" response.body:', statusCode, response.statusMessage, response.body)));

    body = JSON.parse(body);

    var token = {
      jwt: body.access_token,
      expiration: requestTime.add(body.expires_in, 'seconds')
    };

    deferred.resolve(token);
  });

  return deferred.promise;
}

Box.prototype.upload = function upload(filepath, recipient, logger) {
  logger.info('Uploading file %s', filepath);

  if (!filepath) return Q.reject(new Error('missing parameter: filepath'));
  if (!recipient) return Q.reject(new Error('missing parameter: recipient'));
  if (!doesFileExist(filepath)) return Q.reject(new Error(util.format('file does not exist:', filepath)));

  var folderId = recipient.folderId;
  var fileId = recipient.fileId;

  if (!folderId) return Q.reject(new Error('missing parameter: recipient.folderId'));
  if (!fileId) return Q.reject(new Error('missing parameter: recipient.fileId'));

  logger.info('Getting user token');

  return this.getToken().then(function(token) {
    logger.info('Transferring file to Box (%s/%s)', fileId, folderId);

    var url = util.format('https://upload.box.com/api/2.0/files/%s/content', fileId);
    var headers = { 'Authorization': 'Bearer ' + token.jwt };
    var deferred = Q.defer();

    var r = request.post({url: url, headers: headers }, function(err, response) {
        if (err)
          return deferred.reject(new Error(err));

        if (typeof response === 'undefined' || response === null)
          return deferred.reject(new Error('got null response from Box'));

        if (response.statusCode >= 200 && response.statusCode < 300) {
          logger.info('File uploaded (HTTP %s %s)', response.statusCode, response.statusMessage);
          deferred.resolve(filepath);
        }
        else {
          return deferred.reject(new Error('Failed to upload file to Box: HTTP ' + response.statusCode + ' ' + response.statusMessage));
        }
      });

    var form = r.form();
    form.append('folder_id', folderId);
    form.append("filename", fs.createReadStream(filepath));

    return deferred.promise;
  });
};

function rndstr(length) {
  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var str = '';
  for(var i = 0; i < length; i++) {
    str += chars[parseInt(Math.random() * chars.length)];
  }
  return str;
}

function doesFileExist(filePath) {
  try { return fs.statSync(filePath).isFile(); }
  catch (err) { return false; }
}

module.exports = Box;
