var Http = require('./http-client.js');

function isEmpty(v) {
  return typeof v === 'undefined' || v === null || v === '';
}

var EventHubApi = function EventHubApi(settings, token) {
  if (isEmpty(settings)) throw 'Parameter is null or empty: configuration';
  if (isEmpty(token)) throw 'Parameter is null or empty: configuration.token';

  this.settings = settings;
  this.http = new Http(token);
}

EventHubApi.prototype.publish = function publish(name, data) {
  return this.http.post(this.settings, '/' + name, data);
}

module.exports = EventHubApi;
