var Http = require('./http-client.js');
var util = require('util');

var Api = function(settings, token) {
  if (typeof settings === 'undefined' || settings == null)
    throw 'Parameter cannot be null: configuration';

  this.settings = settings;
  this.http = new Http(token);
};

Api.prototype.get = function getSchedule(key) {
  return this.http.get(this.settings, util.format('/%s', key)).
    then(function(response) {
      return response.body;
    });
};

module.exports = Api;
