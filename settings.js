function required(name) {
  var value = process.env[name];
  if (typeof value === 'undefined' || value === null || value.length === 0)
    throw 'Required environment variable not set: ' + name;
  return value;
}

function optional(name, defaultValue) {
  return process.env[name] || defaultValue;
}

module.exports = {
  token: required('LOANTAPEJOBS_TOKEN'),
  services: {
    loantape: {
      host: optional('LOANTAPEJOBS_LOANTAPE_HOST', 'loantape'),
      port: optional('LOANTAPEJOBS_LOANTAPE_PORT', '5000')
    },
    configuration: {
      host: optional('LOANTAPEJOBS_CONFIGURATION_HOST', 'configuration'),
      port: optional('LOANTAPEJOBS_CONFIGURATION_PORT', '5000')
    },
    eventhub: {
      host: optional('LOANTAPEJOBS_EVENTHUB_HOST', 'eventhub'),
      port: optional('LOANTAPEJOBS_EVENTHUB_PORT', '5000')
    }
  }
};
