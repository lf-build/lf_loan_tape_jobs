var jobs = require('./daily-upload-jobs');
var express = require('express');
var Logger = require('./lib/logger');
var app = express();
var port = 5000;

var logger = new Logger('WebApi');

module.exports = {
  run: function run() {
    app.put('/run/:jobName', function (req, res) {
      var jobName = req.params.jobName;
      logger.info('Job run requested: %s', jobName);
      jobs.run(jobName);
      res.send('');
    });

    app.listen(port, function () {
      logger.info('Web server is listening on port %d', port);
    });
  }
};
