var moment = require('moment');
var fs = require('fs');
var os = require('os');
var path = require('path');
var util = require('util');
var settings = require('./settings');
var cron = require('cron');
var CronJob = cron.CronJob;
var ConfigurationApi = require('./lib/configuration-api');
var EventHubApi = require('./lib/eventhub-api');
var Http = require('./lib/http-client');
var Box = require('./lib/box-api');
var Logger = require('./lib/logger');

var configurationApi = new ConfigurationApi(settings.services.configuration, settings.token);
var eventHubApi = new EventHubApi(settings.services.eventhub, settings.token);

var jobs = [];

function addJob(cfg) {
  var logger = new Logger(cfg.name);
  
  var fn = function() {
    logger.info('Running job');
    uploadTapeToBox(cfg.name, cfg.tapeUri, cfg.boxDestination);
  }

  jobs.push({
    cfg: cfg,
    run: fn,
    logger: logger
  });
}

function runJob(name) {
  var job = jobs.find(job => job.cfg.name === name);

  if (!job)
    throw new Error('job not found: ' + name);

  job.run();
}

function scheduleJobs() {
  var context = {};
  var logger = new Logger("Scheduler");

  logger.info("Fetching tenant configuration");

  function storeTenantTimezone(tenantConfig) {
    context.timezone = tenantConfig.timezone;
  }

  function getJobConfigs() {
    return configurationApi.get('loan-tape/jobs');
  }

  configurationApi.get('tenant')
    .then(storeTenantTimezone)
    .then(getJobConfigs)
    .then(configs => configs.forEach(addJob))
    .then(() => jobs.forEach(scheduleJob))
    .done();

  function scheduleJob(job) {
    job.logger.info('Scheduling job "%s": tapeUri="%s" schedule="%s" timezone=%s', job.cfg.name, job.cfg.tapeUri, job.cfg.schedule, context.timezone);
    new CronJob(job.cfg.schedule, job.run, null, true, context.timezone);
  }
}

function uploadTapeToBox(tapeName, tapeUri, boxDestination) {
  var context = {};
  var http = new Http(settings.token);
  var logger = new Logger(tapeName);

  function storeBoxConfig(boxConfig) {
    context.boxConfig = boxConfig;
  }

  function fetchTape() {
    return http.get(settings.services.loantape, tapeUri);
  }

  function writeTapeToDisk(response) {
    var tape = response.body;
    return writeToTempFile(tape, logger);
  }
  
  function uploadToBox(file) {
    var config = context.boxConfig;
    var box = new Box({
      userId: config.userId,
      client: {
        id: config.clientId,
        secret: config.clientSecret
      },
      keys: {
        id: config.publicKeyId,
        prv: base64.decode(config.privateKey)
      },
      timezone: config.timezone
    });
    return box.upload(file, boxDestination, logger);
  }

  function deleteFile(file) {
    logger.info('Deleting local file %s', file);
    fs.unlinkSync(file);
  }

  function publishSuccessEvent() {
    var eventName = 'TapeUploaded';
    logger.info('Publishing %s event', eventName);
    eventHubApi.publish(eventName, {
      tapeName: tapeName,
      tapeUri: tapeUri
    });
  }

  function onError(error) {
    logger.error('Job "%s" failed:', tapeName, error);
    var eventName = 'TapeUploadFailed';
    logger.info('Publishing %s event', eventName);
    eventHubApi.publish(eventName, {
      tapeName: tapeName,
      tapeUri: tapeUri,
      error: error
    });
  }

  configurationApi.get('loan-tape/box')
    .then(storeBoxConfig)
    .then(fetchTape)
    .then(writeTapeToDisk)
    .then(uploadToBox)
    .then(deleteFile)
    .then(publishSuccessEvent)
    .fail(onError)
    .done();
}

var base64 = {
  decode: function (encoded) {
    return new Buffer(encoded || '', 'base64').toString('utf8');
  }
};

function writeToTempFile(content, logger) {
  var dir = os.tmpdir();

  if (!dirExists(dir))
    fs.mkdirSync(dir);
  
  var file = path.join(dir, util.format('daily_loan_tape_%d.csv', new Date().getTime()));
  
  logger.info('Writing tape to file %s', file);

  fs.writeFileSync(file, content);

  return file;
}

function dirExists(filePath) {
  try { return fs.statSync(filePath).isDirectory(); }
  catch (err) { return false; }
}

module.exports = {
  schedule: scheduleJobs,
  run: runJob
};
